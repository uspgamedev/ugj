extends Node

var event_dictionaries = null # Dictionary
var current_event = null # Dictionary
var event_pool = null # Array

func _ready():
	event_dictionaries = DataLoader.get_valid_indexed_dictionaries(
		DataLoader.matrix_to_dictionaries(DataLoader.tsv_to_matrix(
		DataLoader.load_as_text("res://data/events.tsv"))))
		
	$DialogTest.connect("choice_chosen", self, "_on_choice_chosen")
	
	event_pool = get_events_with_order(1, event_dictionaries)
	event_pool.shuffle()
	display_event(event_pool.pop_front())

func display_event(event):
	if event == null:
		show_dialog("Obrigado por jogar!")
		$DialogTest.set_choice_button_states(false)
		return
	
	current_event = event
	show_dialog(current_event["TEXTO"])
	$DialogTest.set_choice_button_states(true, current_event["OP1_TEXTO"],
		current_event["OP2_TEXTO"])
	$DialogTest.set_character(current_event["PERSONAGEM"])
	
func show_dialog(text: String) -> void:
	var text_array = text.split("@")
	for text in text_array:
		$DialogTest.dialog_screens.append(text)
	$DialogTest.show_next_screen()
	print($DialogTest.dialog_screens)
	
func get_events_with_order(order: int, event_dictionaries : Dictionary) -> Array:
	var return_dictionaries = []
	for event in event_dictionaries.values():
		if event["ORDEM"] == str(order):
			return_dictionaries.append(event)
	return return_dictionaries
	
	
func _on_choice_chosen(choice_number: int) -> void:
	var field_name := "OP" + str(choice_number) + "_SEGUINTE_ID"
	var next_id = current_event[field_name]
	if next_id:
		event_pool.append(event_dictionaries[next_id])
	display_event(event_pool.pop_front())
