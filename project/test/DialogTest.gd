extends Control

signal choice_chosen

onready var tie : TextInterfaceEngine = $Panel/TextInterfaceEngine
onready var next_button : Button = $NextButton
onready var previous_button : Button = $PreviousButton
onready var choice_1 : Button = $Choice1
onready var choice_2 : Button = $Choice2

var choices_will_show = false

var dialog_screens := []

var current_screen := -1

func _ready():
	tie.set_color(Color.white)
	
	previous_button.connect("pressed", self, "show_previous_screen")
	next_button.connect("pressed", self, "show_next_screen")
	choice_1.connect("pressed", self, "choice_chosen", [1])
	choice_2.connect("pressed", self, "choice_chosen", [2])
	
#	set_screen_text()
#	update_button_states()
#	set_choice_button_states(false)

func show_next_screen():
	current_screen = min(current_screen + 1, dialog_screens.size() - 1)
	set_screen_text()
	set_choice_button_visibility(false)
	update_button_states()
	

func show_previous_screen():
	current_screen = max(current_screen - 1, 0)
	set_screen_text()
	update_button_states()

func set_screen_text():
	tie.reset()
	tie.buff_text(dialog_screens[current_screen], 0.01)
	tie.set_state(tie.STATE_OUTPUT)

func update_button_states():
	next_button.visible = true
	previous_button.visible = true
	if current_screen == min(current_screen + 1, dialog_screens.size() - 1):
		next_button.visible = false
		
		if choices_will_show:
			set_choice_button_visibility(true)
			
	if current_screen == max(current_screen - 1, 0):
		previous_button.visible = false
			
func set_choice_button_visibility(enable: bool) -> void:
	choice_1.visible = enable
	choice_2.visible = enable
		
		
func set_choice_button_states(enable: bool, text_1 := "", text_2 := ""):
	choices_will_show = enable
	choice_1.text = text_1
	choice_2.text = text_2

func choice_chosen(choice_number : int) -> void:
	emit_signal("choice_chosen", choice_number)

func set_character(character_info: String):
	$Panel/CharacterInfo.text = character_info
	var texture_path = "res://test/" + character_info + ".png"
	var file_to_check := File.new()
	var file_exists = file_to_check.file_exists(texture_path)
	if file_exists:
		$Panel/TextureRect.set_texture(load(texture_path))
	else:
		$Panel/TextureRect.set_texture(null)
