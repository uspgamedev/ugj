extends Node

var time: float
var audio_index: int = 0

func _ready():
	time = $Audios/AudioStreamPlayer.stream.get_length() - 1.0
	$Audios/AudioStreamPlayer.play()
	$Timer.start(time)

func _on_Timer_timeout():
	audio_index = (audio_index + 1) % 2
	$Audios.get_children()[audio_index].play()
	$Timer.start(time)
