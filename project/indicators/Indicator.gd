extends Control

export(String) var indicator_display_name
export(String) var indicator_id

func _ready():
	$Label.text = indicator_display_name
	
	var school_stats:Stats = get_node("/root/SchoolStats")
	school_stats.connect("stat_changed", self, "_on_stat_changed")
	
	# Terrible workaround to the fact that otherwise can't animate colors independently
	$MarginContainer/ProgressBar.add_stylebox_override("fg", StyleBoxFlat.new())
	$MarginContainer/ProgressBar.get_stylebox("fg").bg_color = Color.white
	
func _on_stat_changed(stat_name:String, new_value:int):
	if stat_name == indicator_id:
		var delta = new_value - $MarginContainer/ProgressBar.value
		if delta != 0:
			$AnimationPlayer.play("increase" if delta > 0 else "decrease")
			$MarginContainer/ProgressBar.value = new_value # TODO animate neatly
