class_name DataLoader

extends Node
	
static func load_as_text(file_path: String) -> String:
	"""
	Reads file at file_path and returns its contents as a String
	"""
	
	var file = File.new()
	file.open(file_path, file.READ)
	var content = file.get_as_text()
	file.close()
	return content
	
	
static func tsv_to_matrix(contents: String) -> Array:
	"""
	Separates the contents string in rows and columns returning a bi-dimensional array (matrix).
	"""
	
	var return_matrix = []
	for line in contents.split("\n"):
		var line_array = []
		for column in line.split("\t"):
			line_array.append(column)
		return_matrix.append(line_array)
	return return_matrix
	
	
static func matrix_to_dictionaries(matrix: Array) -> Array:
	"""
	Uses the first row as the keys for the dictionaries that will contain each column data of each
	row starting from the second one.
	"""
	
	var return_dictionaries = []
	var header_row = null
	
	for row in matrix:
		if header_row == null: # Do this once, so it captures only the first row
			header_row = row
			continue
		
		var row_dictionary = {}
		for i in range(0, row.size()):
			row_dictionary[header_row[i]] = row[i]
		return_dictionaries.append(row_dictionary)
	
	return return_dictionaries
	
	
static func get_valid_indexed_dictionaries(dictionaries: Array) -> Dictionary:
	"""
	Scans dictionaries and 
	"""
	
	var return_dictionaries := {}
	var mandatory_fields = ["TEXTO", "OP1_TEXTO", "OP1_TEXTO"] # Except ID that is a special use case
	for dictionary in dictionaries:
		if not dictionary["ID"]:
#			print("Ignoring dictionary that has no ID: ", dictionary)
			continue
		
		var continue_outer_loop = false
		for field in mandatory_fields:
			if not dictionary[field]:
#				print("Ignoring dictionary with ID ", dictionary["ID"], " because it does not have field ", field)
				continue_outer_loop = true
				break
		
		if continue_outer_loop:
			continue
		
		return_dictionaries[dictionary["ID"]] = dictionary
	return return_dictionaries
	
