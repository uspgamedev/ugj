extends Node

onready var debug_mode : bool = get_parent().debug_mode
var event_dictionaries : Dictionary

func _ready():
	if debug_mode:
		event_dictionaries = DataLoader.get_valid_indexed_dictionaries(
		DataLoader.matrix_to_dictionaries(DataLoader.tsv_to_matrix(
		DataLoader.load_as_text("res://data/events-tmp.tsv"))))
	else:
		event_dictionaries = DataLoader.get_valid_indexed_dictionaries(
		DataLoader.matrix_to_dictionaries(DataLoader.tsv_to_matrix(
		DataLoader.load_as_text("res://data/events.tsv"))))

func get_events_with_order(order: int) -> Array:
	var return_dictionaries = []
	for event in event_dictionaries.values():
		if event["ORDEM"] == str(order):
			return_dictionaries.append(event)
	return return_dictionaries

func get_all_events_from_subtree(event_id: int) -> Array:
	var return_events := [event_id]
	for event in event_dictionaries.values():
		if int(event["ID"]) == event_id:
			if event["OP1_SEGUINTE_ID"]:
				return_events += get_all_events_from_subtree(int(event["OP1_SEGUINTE_ID"]))
			if event["OP2_SEGUINTE_ID"]:
				return_events += get_all_events_from_subtree(int(event["OP2_SEGUINTE_ID"]))
	return return_events
