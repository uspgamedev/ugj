extends Node
class_name Stats

signal stat_changed(stat_name, new_value)

const LOW_DISCIPLINE = 1
const HIGH_DISCIPLINE = 2
const LOW_CONVIVENCE = 3
const HIGH_CONVIVENCE = 4
const LOW_ENGAGEMENT = 5
const HIGH_ENGAGEMENT = 6
const NO_PROBLEM = 0

var difficulty # This var is here temporarily because this class is the only autoload

var stats: Dictionary = {
	"Discipline" : {"MAX_VALUE" : 0, "MIN_VALUE" : 0, "POINTS" : 0},
	"Convivence" : {"MAX_VALUE" : 0, "MIN_VALUE" : 0, "POINTS" : 0},
	"Engagement" : {"MAX_VALUE" : 0, "MIN_VALUE" : 0, "POINTS" : 0},
	"Learning" : {"MAX_VALUE" : 0, "MIN_VALUE" : 0, "POINTS" : 0}
}

func update_stats(discipline_mod: int ,
					convivence_mod: int , 
						engagement_mod: int ,
							learning_mod: int ) -> void:
	print(discipline_mod)
	print(convivence_mod)
	print(engagement_mod)
	print(learning_mod)
	stats["Discipline"]["POINTS"] += discipline_mod
	stats["Convivence"]["POINTS"] += convivence_mod
	stats["Engagement"]["POINTS"] += engagement_mod
	stats["Learning"]["POINTS"] += learning_mod
	for status_name in stats.keys():
		clamp_stats(status_name)
	update_score_board()
	
	for stat in ["Discipline", "Convivence", "Engagement", "Learning"]:
		emit_signal("stat_changed", stat, stats[stat]["POINTS"])
	
	print_stats()

func set_values(stats_name:String, max_value:int, min_value:int, starting_value:int) -> void:
#	if not stats_name in stats.keys():
#		print("ERROR:Stats nonexistent")
#		return
	stats[stats_name]["MAX_VALUE"] = max_value
	stats[stats_name]["MIN_VALUE"] = min_value
	stats[stats_name]["POINTS"] = starting_value

func clamp_stats(stats_name: String) -> void:
#	if not stats_name in stats.keys():
#		print("ERROR:Stats nonexistent")
#		return
	stats[stats_name]["POINTS"] = clamp(stats[stats_name]["POINTS"], stats[stats_name]["MIN_VALUE"], stats[stats_name]["MAX_VALUE"])

func get_percentage(stats_name:String) -> float:
	return ((float(stats[stats_name]["POINTS"]) - float(stats[stats_name]["MIN_VALUE"]))/(float(stats[stats_name]["MAX_VALUE"]) - float(stats[stats_name]["MIN_VALUE"]))) * 100

func print_stats() -> void:
	for stats_name in stats.keys():
		if stats[stats_name]["MAX_VALUE"] != 0:
			print(str(get_percentage(stats_name)) + "%")
		else:
			print("stats[" + stats_name + "][MAX_VALUE] = 0\n")
	print("------------------------------------------------------------")
	pass

func update_score_board() -> void:
	if not get_tree().get_nodes_in_group("score_paper").empty():
		var score_paper = get_tree().get_nodes_in_group("score_paper")[0]
		score_paper.update_score(get_percentage("Discipline"), get_percentage("Convivence"), get_percentage("Engagement"), get_percentage("Learning"))
	else:
		print("score_paper not found")
