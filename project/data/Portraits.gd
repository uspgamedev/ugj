extends Reference
class_name Portraits

const REFS = {
	"Professor de Matemática":
		"res://assets/images/characters/3.png",
	"Soraya Magalhães":
		"res://assets/images/characters/25.png",
	"Professora de Língua Portuguesa":
		"res://assets/images/characters/4.png",
	"João Silva (Professor)":
		"res://assets/images/characters/9.png",
	"Alberto Alves":
		"res://assets/images/characters/2.png",
	"Diretoria de Ensino":
		"res://assets/images/characters/13.png",
	"Remildo Bolsan Jr. (Presidente do Grêmio)":
		"res://assets/images/characters/6.png",
	"Adriano Leite Ribeiro (Aluno da turma 7°B)":
		"res://assets/images/characters/8.png",
	"Professor de Educação Física":
		"res://assets/images/characters/10.png",
	"Professora de Artes":
		"res://assets/images/characters/5.png",
	"Cão":
		"res://assets/images/characters/dog.png",
}
