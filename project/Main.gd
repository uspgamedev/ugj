extends Control

export(bool) var debug_mode

export(int) var starting_discipline
export(int) var max_possible_discipline
export(int) var min_possible_discipline

export(int) var starting_convivence
export(int) var max_possible_convivence
export(int) var min_possible_convivence

export(int) var starting_engagement
export(int) var max_possible_engagement
export(int) var min_possible_engagement

export(int) var starting_learning_rate
export(int) var max_possible_learning_rate
export(int) var min_possible_learning_rate

onready var school_stats:Stats = get_node("/root/SchoolStats")
onready var event_db = $EventDB
onready var dialog_window = $DialogWindow
onready var previous_button = $PreviousButton
onready var next_button = $NextButton
onready var choice_1 = $Choice1
onready var choice_2 = $Choice2
onready var event_timer : Timer = $EventTimer
onready var room : Room = $Room
onready var hud_elements := [$DialogWindow, $PreviousButton, $NextButton, $Choice1, $Choice2]

const CHOICE_BUTTONS_Y = [456, 390]

var events_lost := []
var event_pool : Array
var chapter := 0

func _ready():
	school_stats.set_values("Discipline", max_possible_discipline, min_possible_discipline, starting_discipline)
	school_stats.set_values("Convivence", max_possible_convivence, min_possible_convivence, starting_convivence)
	school_stats.set_values("Engagement", max_possible_engagement, min_possible_engagement, starting_engagement)
	school_stats.set_values("Learning", max_possible_learning_rate, min_possible_learning_rate, starting_learning_rate)
	
	previous_button.connect("pressed", self, "_on_previous_pressed")
	next_button.connect("pressed", self, "_on_next_pressed")
	
	choice_1.connect("pressed", self, "choice_chosen", [1])
	choice_2.connect("pressed", self, "choice_chosen", [2])
	
	hide_choices()
	previous_button.hide()
	next_button.hide()
	dialog_window.ask_for_name()
	
	if SchoolStats.difficulty == "normal":
		$Indicators.hide()

func hide_hud_elements():
	var tween : Tween = Tween.new()
	add_child(tween)
	for hud_element in hud_elements:
		tween.interpolate_property(hud_element, "modulate:a", hud_element.modulate.a, 0.0, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	dialog_window.clear_screen()
	tween.queue_free()

func show_hud_elements():
	var tween : Tween = Tween.new()
	add_child(tween)
	for hud_element in hud_elements:
		tween.interpolate_property(hud_element, "modulate:a", hud_element.modulate.a, 1.0, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	tween.queue_free()

func name_confirmed(name: String):
	if name == "":
		name = "Coordenadora"
	dialog_window.user_name = name
	dialog_window.tie.set_process_input(false)
	yield(wait_for_next_event(), "completed")
	start_next_chapter()

func start_next_chapter():
	chapter += 1
	var all_events = event_db.get_events_with_order(chapter)
	for event in all_events:
		if not event in events_lost:
			event_pool.append(event)
	if event_pool.empty():
		hide_choices()
		next_button.hide()
		previous_button.hide()
		# game content has ended
		return
	randomize()
	event_pool.shuffle()
	start_next_event()

func start_next_event():
	var event = event_pool.pop_front()
	if event == null:
		show_score()
		return
	dialog_window.set_current_event(event)
	choice_1.text = event["OP1_TEXTO"]
	choice_2.text = event["OP2_TEXTO"]
	hide_choices()
	update_screen_buttons()
	dialog_window.show_current_screen()

func hide_choices():
	choice_1.hide()
	choice_2.hide()

func show_choices():
	var rand_index = randi() % 2
	choice_1.rect_position.y = CHOICE_BUTTONS_Y[rand_index]
	choice_1.show()
	choice_2.rect_position.y = CHOICE_BUTTONS_Y[(rand_index + 1) % 2]
	choice_2.show()

func choice_chosen(choice_number : int):
	var event = dialog_window.current_event
	school_stats.update_stats(int(event["OP" + str(choice_number) + "_DISCIPLINA"]), 
								int(event["OP" + str(choice_number) + "_CONVIVENCIA"]), 
									int(event["OP" + str(choice_number) + "_ENGAJAMENTO"]), 
										int(event["OP" + str(choice_number) + "_APRENDIZADO"]))
	dialog_window.show_feedback(choice_number)
	hide_choices()
	next_button.show()
	previous_button.hide()
	var id_field = str("OP", str(1 + (choice_number % 2)), "_SEGUINTE_ID")
	events_lost += event_db.get_all_events_from_subtree(int(dialog_window.current_event[id_field]))

func _on_previous_pressed():
	dialog_window.show_previous_screen()
	next_button.show()
	update_screen_buttons()

func wait_for_next_event():
	yield(hide_hud_elements(), "completed")
	event_timer.start()
	yield(event_timer, "timeout")
	room.blink_door()
	event_timer.stop()
	yield(room, "door_pressed")
	yield(show_hud_elements(), "completed")

func _on_next_pressed():
	if dialog_window.is_showing_feedback:
		dialog_window.is_showing_feedback = false
		yield(wait_for_next_event(), "completed")
		start_next_event()
	else:
		dialog_window.show_next_screen()
		previous_button.show()
		update_screen_buttons()

func update_screen_buttons():
	previous_button.show()
	next_button.show()
	if dialog_window.is_last_screen():
		show_choices()
		next_button.hide()
	if dialog_window.is_first_screen():
		previous_button.hide()

func hide_game() -> void:
	$DialogWindow/Panel.hide()
	$PreviousButton.hide()
	$NextButton.hide()
	hide_choices()

func show_game() -> void:
	$DialogWindow/Panel.show()
	$PreviousButton.show()
	$NextButton.show()
	$Choice1.show()
	$Choice2.show()

func show_score():
	hide_game()
	$ScorePaper.show()
	yield($ScorePaper/Button,"pressed")
	$ScorePaper.hide()
	show_game()
	start_next_chapter()
