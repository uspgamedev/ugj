extends Control
class_name Room

signal door_pressed
onready var door : TextureRect = $Door
onready var door_timer : Timer = $Door/Timer

func _ready():
	door.get_node("TextureButton").connect("pressed", self, "on_door_pressed")
	door_timer.connect("timeout", self, "blink_finished")

func blink_door():
	var tween = door.get_node("Tween")
	tween.interpolate_property(door, "modulate", door.modulate, Color(2.0, 2.0, 2.0), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property(door, "modulate", Color(2.0, 2.0, 2.0), Color(1.0, 1.0, 1.0), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN, .5)
	tween.start()
	door_timer.start()

func blink_finished():
	blink_door()

func stop_door_blink():
	door.get_node("Tween").reset_all()
	door.get_node("Tween").stop_all()
	door_timer.stop()
	door.modulate = Color(1.0, 1.0, 1.0)

func on_door_pressed():
	stop_door_blink()
	var instance = load("res://popout/CanComeInPopOut.tscn").instance()
	instance.rect_position = get_global_mouse_position()
	add_child(instance)
	yield(get_tree().create_timer(0.6), "timeout")
	emit_signal("door_pressed")
