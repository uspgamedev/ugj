extends Control

func _ready():
	$VBoxContainer/ButtonEasy.connect("pressed", self, "_on_button_pressed", ["easy"])
	$VBoxContainer/ButtonNormal.connect("pressed", self, "_on_button_pressed", ["normal"])
	
func _on_button_pressed(difficulty):
	SchoolStats.difficulty = difficulty
	get_tree().change_scene("res://Main.tscn")
