extends TextureRect

onready var first_chapter: bool = true

func update_score(discipline: float, convivence: float, engagement: float, learning: float):
	print(discipline)
	print(convivence)
	print(engagement)
	print(learning)
	$VSplitContainer/Disciplina/Percentage.text = str(discipline) + "%"
	$VSplitContainer/Convivencia/Percentage.text = str(convivence) + "%"
	$VSplitContainer/Engajamento/Percentage.text = str(engagement) + "%"
	$VSplitContainer/Aprendizado/Percentage.text = str(learning) + "%"
