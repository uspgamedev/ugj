extends Control

onready var tie : TextInterfaceEngine = $Panel/TextInterfaceEngine
onready var character_info : Label = $Panel/CharacterInfo
onready var portrait : TextureRect = $Panel/CharacterPicture

var current_event : Dictionary
var current_screen : int
var screen_texts : Array
var is_showing_feedback := false
var user_name : String

func set_current_event(event : Dictionary):
	var fields = ["TEXTO", "OP1_DEVOLUTIVA", "OP2_DEVOLUTIVA"]
	current_event = event
	current_screen = 0
	for field in fields:
		event[field] = event[field].replace("{nome}", user_name)
	screen_texts = event["TEXTO"].split("@")
	character_info.text = event["PERSONAGEM"]
	if character_info.text.ends_with(" "):
		character_info.text = character_info.text.substr(0, character_info.text.length() - 1)
	if Portraits.REFS.has(character_info.text):
		portrait.texture = load(Portraits.REFS[character_info.text])
	else:
		portrait.texture = null

func show_current_screen():
	set_screen_text()

func show_next_screen():
	current_screen = min(current_screen + 1, screen_texts.size() - 1)
	set_screen_text()

func show_previous_screen():
	current_screen = max(current_screen - 1, 0)
	set_screen_text()

func set_screen_text():
	set_tie_text(screen_texts[current_screen])

func is_last_screen():
	return current_screen == screen_texts.size() - 1

func is_first_screen():
	return current_screen == 0

func show_feedback(choice_number : int):
	var field_name = str("OP", choice_number, "_DEVOLUTIVA")
	set_tie_text(current_event[field_name])
	is_showing_feedback = true

func clear_screen():
	set_tie_text("")
	character_info.text = ""
	portrait.texture = null

func set_tie_text(text):
	tie.reset()
	
	var parts = [text]
	var pause_characters = [",", "! ", "? ", ". "]
	
	for pause_character in pause_characters:
		var new_parts = []
		for part in parts:
			var sub_parts = part.split(pause_character)
			for i in range(0, sub_parts.size() - 1):
				sub_parts[i] += pause_character
			new_parts = PoolStringArray(new_parts) + sub_parts
		parts = new_parts
		
	for part in parts:
		if part:
			tie.buff_text(part, 0.01)
			tie.buff_silence(0.4)
	
	tie.set_state(tie.STATE_OUTPUT)

func ask_for_name():
	tie.connect("input_enter", get_parent(), "name_confirmed")
	tie.buff_text("Qual é o seu nome?\n", 0.01)
	tie.buff_input()
	tie.set_state(tie.STATE_OUTPUT)
