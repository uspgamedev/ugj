# USP Game Jam - 10 Years Anniversary
November 2019

You are a pedagogic coordinator in a school. You are working. There are attributes in the school:

- Discipline
- Engagement
- Convivence

The indicators are hidden to the player or not, according to the difficulty mode.

When you are working, there are situation that appear to you and there are choices that you can make for each situation. Every option you do, increases an attribute and diminishes another. When any attributes gets to the minimum or maximum, something bad happens and you may loose.

The graphic interface for the game is a still image first-person in a desk with a chair where people will enter and talk with you.
On top of it, there is a dialog box where the text appears, buttons to continue to the next/previous dialog, and two buttons below to make your choice that will only appear when necessary.

The situations that appear to you are made of a decision tree with every possibility, and the situations can get paused and resumed at a later point with a different dialog of the same situation. That way, multiple situation trees can be happening at the same time, intercalating dialogs of each situation.

When a non-first dialog of a situation appears, the user may select an option to review the notes of previous dialogs of the same situation. It will appear as a paper sheet with hand-written notes with the dialogs and choices the user made. The user can close it and return to the on-going dialog.

There is also a report card screen, that will appear at the end of every semester. A semester is like a "Chapter" of the game.

There is a profiles ui with data on the "Turmas" and professors, that the user can access at any point to help make the right decision in the choices.

A mini-game that is a nice-to-have is a puzzle with the schedules of the professors. The user needs to fit the classes in the available slots of the professor. Every class should not have overlapping professors at any time.

## Data

The events data is done in a Google Spreadsheet which is exported as .tsv and placed in project/data/events.tsv.

The texts may have certain special words:

### @

Divides dialog text.

### {nome}

Name of the coordinator.

### *...

Description without avatar.
